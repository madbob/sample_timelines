# GLOCAL HISTORY

Here are hosted the data for the "glocal timelines" of the history of Linux community around the world.

Folders structure:

 - data: contains the actual JSON files, one for each geographical area (starting from the `world.json` file, which contains the global milestones). Each folder may contain a "image" folder, for the images referenced by the data file, and a "children" folder, which recursively permits to include more specific areas within the parent one (e.g. "italia", for national milestones, which contains "torino", for city-level milestone)
 - scripts: a few utilities, to parse and transform the data files in something more "friendly" and usable
