<?php

/*
    Converts an internal JSON file into a proper Timeline.JS file, filtering
    texts for the choosen language.

    Example:
    php scripts/to_timelinejs.php data/children/italia/italia.json it
*/

$filepath = $argv[1];
$lang = $argv[2];

$contents = json_decode(file_get_contents($filepath));

foreach($contents->events as $event) {
    if (isset($event->text->$lang)) {
        $event->text = $event->text->$lang;
    }
    else {
        unset($event);
    }
}

echo json_encode($contents, JSON_PRETTY_PRINT);
